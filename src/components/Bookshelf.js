import React from 'react';

class Bookshelf extends React.Component {

    constructor(props){
        super(props)
        this.state = {
          items: [],
          isLoaded: false,
          book: { id:null },
        };
      }
       
    componentDidMount(){
        fetch('http://localhost:1337/testers')
        .then(res => res.json()) 
        .then(json => {
          this.setState({
            isLoaded: true,
            items: json,
          })
        });
    }

    handleBookClick(book) {
        this.setState({book: book});
    }

    handleBackClick() {
        this.setState({book: { id:null }});
    }

    render() {
        var {isLoaded, items, book} = this.state;
        var answer = <div>Loading...</div>;
        console.log(book.id);
        if(book.id!=null){
            answer=<div>
                <p>{book.title}</p>
                <p>{book.author}</p>
                <button onClick={this.handleBackClick.bind(this)}>Back</button>
                </div>;
        }
        else{
            if(isLoaded) answer = <ul>{items.map(item => (
            <li key={item.id} onClick={this.handleBookClick.bind(this, item)}>
                 {item.title}    |    {item.author}  
            </li>
            ))}</ul>;
        }
        return <div>{answer}</div>;
    }
}

export default Bookshelf;
